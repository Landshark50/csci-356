#part A
print("Hello World!")


# part B
def find_GCD(num1, num2):
    smaller_number = int(min(num1, num2))
    i = 1
    while i <= smaller_number:
        if (num1 % i == 0) and (num2 % i == 0):
            common_denom = i
        i += 1
    print(f'The GCD between {num1} and {num2} is {common_denom}')


numbers = input("Enter 2 numbers separated by a space:  ")
x, y = numbers.split()
x = int(x)
y = int(y)
find_GCD(x, y)

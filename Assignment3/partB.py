from StackADT import Stack
from StackADT2 import Stack2
import timeit
from timeit import Timer
import time


def rev_string_A(my_str):

    s = Stack()
    [s.push(letter) for letter in my_str]
    reversed_string_list = []
    while not s.is_empty():
        reversed_string_list.append(s.pop())
    reversed_string = ''.join(reversed_string_list)
    return reversed_string


def rev_string_B(my_str):
    s = Stack2()
    [s.push(letter) for letter in my_str]
    reversed_string_list = []
    while not s.is_empty():
        reversed_string_list.append(s.pop())
    reversed_string = ''.join(reversed_string_list)
    return reversed_string

string1 = 'x' * 500 + 'o' * 500

t1 = Timer(lambda: rev_string_A(string1))
execution_time1 = t1.timeit(number=1000)
print(f"Time for rev_string_A is {execution_time1*100} ms")

t2 = Timer(lambda: rev_string_B(string1))
execution_time2 = t2.timeit(number=1000)
print(f"Time for rev_string_B is {execution_time2*100} ms")

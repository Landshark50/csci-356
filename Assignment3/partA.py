
from StackADT import Stack
from StackADT2 import Stack2


#uses the stack to reverse the characters in a string
def rev_string_A(my_str):
    print('rev_string_A')
    s = Stack()
    [s.push(letter) for letter in my_str]
    print(my_str)
    reversed_string_list = []
    while not s.is_empty():
        reversed_string_list.append(s.pop())
    reversed_string = ''.join(reversed_string_list)
    print(reversed_string + '\n')



def rev_string_B(my_str):
    print('rev_string_B')
    s = Stack2()
    [s.push(letter) for letter in my_str]
    print(my_str)
    reversed_string_list = []
    while not s.is_empty():
        reversed_string_list.append(s.pop())
    reversed_string = ''.join(reversed_string_list)
    print(reversed_string + '\n')


rev_string_A('Hello')
rev_string_B('Hello World')

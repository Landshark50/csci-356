
def gcd(m, n):
    while m % n != 0:
        old_m = m
        old_n = n

        m = old_n
        n = old_m % old_n

    return n

class Fraction:
    def __init__(self, top, bottom):
        common = gcd(top, bottom)
        self.num = top // common
        self.den = bottom // common

    def __str__(self):
        return str(self.num) + " / " + str(self.den)

    def get_num(self):
        return self.num

    def get_den(self):
        return self.den

    def __add__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num = self.get_num() * other.get_den() + self.get_den() * other.get_num()
        new_den = self.get_den() * other.get_den()
        return Fraction(new_num , new_den)

    def __radd__(self, other_integer):
        x = self.__add__(other_integer)
        return x

    def __sub__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num = self.get_num() * other.get_den() - self.get_den() * other.get_num()
        new_den = self.get_den() * other.get_den()
        return Fraction(new_num, new_den)

    def __mul__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num = self.get_num() *  other.get_num()
        new_den = self.get_den() * other.get_den()
        common = gcd(new_num, new_den)
        return Fraction(new_num // common, new_den // common)

    def __truediv__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num = self.get_num() * other.get_den()
        new_den = self.get_den() * other.get_num()
        common = gcd(new_num, new_den)
        return Fraction(new_num // common, new_den // common)

    def __gt__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num1 = self.get_num() * other.get_den()
        new_num2 = other.get_num() * self.get_den()
        if new_num1 > new_num2:
            return True
        else:
            return False

    def __ge__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num1 = self.get_num() * other.get_den()
        new_num2 = other.get_num() * self.get_den()
        if new_num1 >= new_num2:
            return True
        else:
            return False

    def __lt__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num1 = self.get_num() * other.get_den()
        new_num2 = other.get_num() * self.get_den()
        if new_num1 < new_num2:
            return True
        else:
            return False

    def __le__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num1 = self.get_num() * other.get_den()
        new_num2 = other.get_num() * self.get_den()
        if new_num1 <= new_num2:
            return True
        else:
            return False

    def __ne__(self, other):
        if type(other) == int:
            other = Fraction(other, 1)
        new_num1 = self.get_num() * other.get_den()
        new_num2 = other.get_num() * self.get_den()
        if new_num1 != new_num2:
            return True
        else:
            return False


# part C
print('Part C')
print('Inputs are 4/16 and 1/2')
x = Fraction(4, 16) + Fraction(1, 2)
print(f'OUTPUT:  {x}')

print('Inputs are 1/4 and 1')
y = Fraction(1,4) + 1
print(f'OUTPUT:  {y}')

print('Inputs are 2 and 2/5')
z = 2 + Fraction(2, 5)
print(f'OUTPUT:  {z}')

print('')
print('Bonus functions')
print('Multiplication')


print('Division')
print('Input: 5/4 divided by 2')
div = Fraction(5, 4) / 2
print(f'OUTPUT:  {div} \n')

print('Greater Than')
print('Input:  1/2 > 3/4 ?')
greater_than = (Fraction(1,2) > Fraction(3,4))
print(f'OUTPUT:  {greater_than} \n')

print('Greater Than or Equal to')
print('Input:  1/2 >= 2/4 ?')
greater_than_equal_to = (Fraction(1,2) >= Fraction(2,4))
print(f'OUTPUT:  {greater_than_equal_to} \n')

print('Less Than')
print('Input:  1/2 < 3/4 ?')
less_than = (Fraction(1,2) < Fraction(3,4))
print(f'OUTPUT:  {less_than} \n')


print('Less Than or Equal to')
print('Input:  1/2 <= 2/4 ?')
less_than_equal_to = (Fraction(1,2) <= Fraction(2,4))
print(f'OUTPUT:  {less_than_equal_to} \n')

print('Not Equal to')
print('Input:  1/2 != 1/2 ?')
not_equal_to = (Fraction(1,2) != Fraction(2,4))
print(f'OUTPUT:  {not_equal_to} \n')
print('Input:  1/2 != 2 ?')
not_equal_to = (Fraction(1,2) != 2)
print(f'OUTPUT:  {not_equal_to} \n')
print('')




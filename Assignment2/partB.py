
def gcd(m, n):
    while m % n != 0:
        old_m = m
        old_n = n

        m = old_n
        n = old_m % old_n

    return n

class Fraction:
    def __init__(self, top, bottom):
        common = gcd(top, bottom)
        self.num = top // common
        self.den = bottom // common

    def __str__(self):
        return str(self.num) + " / " + str(self.den)

    def get_num(self):
        return self.num

    def get_den(self):
        return self.den

    def __add__(self, other_fraction):
        new_num = self.get_num() * other_fraction.get_den() + self.get_den() * other_fraction.get_num()
        new_den = self.get_den() * other_fraction.get_den()
        return Fraction(new_num , new_den)


# part B
print('Input: 4/16 + 1/2')
x = Fraction(4, 16) + Fraction(1, 2)
print(f'OUTPUT:  {x}')


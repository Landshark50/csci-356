
class Fraction:
    def __init__(self, top, bottom):
        self.num = top
        self.den = bottom

    def __str__(self):
        return str(self.num) + " / " + str(self.den)

    def get_num(self):
        return self.num

    def get_den(self):
        return self.den


    def __add__(self, other_fraction):
        new_num = self.get_num() * other_fraction.get_den() + self.get_den() * other_fraction.get_num()
        new_den = self.get_den() * other_fraction.get_den()
        return Fraction(new_num, new_den)

#part A
print('Input: 1/4 + 1/2')
x = Fraction(1,4) + Fraction(1,2)
print(f'OUTPUT:  {x}')
from UnorderedListADT import UnorderedList


mylist = UnorderedList()
print(f'The list size is now {mylist.size()}')

print('Adding 4 items to the list')
mylist.add(1)
mylist.add(2)
mylist.add(3)
mylist.add(4)

print(f'The list size is now {mylist.size()}')
print('Removing 1 item from the list ')

mylist.remove(2)

print(f'The list size is now {mylist.size()}')


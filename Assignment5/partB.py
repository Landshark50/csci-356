from UnorderedListADT import UnorderedList
from matplotlib import pyplot as plt
from timeit import Timer

mylist = UnorderedList()

def measure_append_time(my_list):
    #my_list = UnorderedList()
    my_list.append(1)

t1 = Timer(lambda: measure_append_time(mylist))
x = 1
execution_times_list = []
while x < 2502:
    execution_time = t1.timeit(number=x)
    execution_times_list.append(execution_time)
    x += 100
    print(x)

plt.plot(execution_times_list)
plt.xlabel(xlabel='Iterations (hundreds)')
plt.ylabel(ylabel='Time to run (seconds)')
plt.title('Big O Notation Time 1')
plt.show()
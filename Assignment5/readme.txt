For part A, I was asked to change the size method to store the number of
nodes in the list as an additional piece of data. To do this, we create a 
new variable in our constructor called "self.count_nodes" which keeps track
of the number of nodes within the list. The initial value of this variable
is zero. Then, I added code to increment count_nodes within the add function 
and to decrement count_nodes in the remove function. The size method was 
changed to just return the value of the count_nodes variable.

For part B, I was asked to implement the append method and see what the big O notation was like. My original written code gave me O(n^2) and a graph of the
execution times is shown in partBoutput1.png. This was due to the while loop in the function. After commenting this loop out and creating a new variable
that keeps track of the end of the list, the new append function was 
modified, and its output is shown in partBoutput2.png. As you can see, the
graph appears to be O(n), but the variation in time is so small that the
function can be approximated as O(1).

For part C, I was asked to modify the remove method so it can work if I ask
the program to remove an item that isn't in the list. A new function called
"remove_new" was made to accomplish this. The first while loop was modified
to include 2 conditions, the first being the original "not found" condition 
in the original function, the second being "current is not None." we do this 
to prevent the function from raising an error if the item is not in the 
list. 
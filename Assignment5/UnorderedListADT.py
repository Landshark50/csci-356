from NodeADT import Node

class UnorderedList:

    def __init__(self):
        self.head = None
        self.end = None
        self.count_nodes = 0

    def is_empty(self):
        return self.head == None

    def add(self, item):
        temp = Node(item)
        temp.set_next(self.head)
        self.head = temp
        self.count_nodes += 1

    def size(self):
        return self.count_nodes

    def search(self,item):
        current = self.head
        found = False
        while current != None and not found:
            if current.get_data() == item:
                found = True
            else:
                current = current.get_next()

        return found

    def remove(self, item):
        current = self.head
        previous = None
        found = False
        while not found:
            if current.get_data() == item:
                found = True
            else:
                previous = current
                current = current.get_next()

        if previous == None:
            self.head = current.get_next()
        else:
            previous.set_next(current.get_next())

        self.count_nodes -= 1

    def remove_new(self, item):
        current = self.head
        previous = None
        found = False
        while current is not None and not found:
            if current.get_data() == item:
                found = True
            else:
                previous = current
                current = current.get_next()

        if found:
            if previous is None:
                self.head = current.get_next()
            else:
                previous.set_next(current.get_next())
            self.count -= 1
        else:
            print("Item not found in the list.")

    def append(self, item):
        temp = Node(item)
        if self.head is None:
            self.head = temp
            self.end = temp
        else:
            current = self.head
            self.end.set_next(temp)
            self.end = temp
            while current.get_next() is not None:
                current = current.get_next()
            current.set_next(temp)
        self.count_nodes += 1

    def append2(self, item):
        temp = Node(item)
        if self.head is None:
            self.head = temp
            self.end = temp
        else:
            current = self.head
            self.end.set_next(temp)
            self.end = temp
      #      while current.get_next() is not None:
         #       current = current.get_next()
       #     current.set_next(temp)
        self.count_nodes += 1
